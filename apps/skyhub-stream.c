#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>

#include <skyhub-stream.h>

static gchar *camera_type = NULL;
static gchar **sources = NULL;
static char *model = NULL;

static GOptionEntry entries[] =
{
	{ "camera-type", 't', 0, G_OPTION_ARG_STRING, &camera_type, "Type of Camera", "fisheye|ptz" },
	{ "source", 's', 0, G_OPTION_ARG_STRING_ARRAY, &sources, "Video Source", "uri://path/to/source" },
	{ "model", 'm', 0, G_OPTION_ARG_STRING_ARRAY, &model, "Detection Model", "primary" },
	{ NULL }
};

int main(int argc, char *argv[])
{
	int rc = 0;
	GMainLoop *loop = NULL;
	skyhub_stream_t *stream = NULL;
	int num_sources = argc - 1;
	int i;
	GError *error = NULL;
	GOptionContext *context;

	context = g_option_context_new ("");
	g_option_context_add_main_entries (context, entries, NULL);
	if (!g_option_context_parse (context, &argc, &argv, &error))
	{
		g_print ("option parsing failed: %s\n", error->message);
		exit (1);
	}

	/* Standard GStreamer initialization */
	gst_init (&argc, &argv);
	loop = g_main_loop_new (NULL, FALSE);

	stream = skyhub_stream_new(SKYHUB_STREAM_TYPE_FISHEYE, loop, "primary");

	g_assert(stream != NULL);

	for (char **src = sources; src != NULL && *src != NULL; src++) {
		printf("Source Added: %s\n", *src);
		skyhub_stream_source_add(stream, *src);
		break;
	}

	rc = skyhub_stream_start(stream);

	/* Wait till pipeline encounters an error or EOS */
	g_print ("Running...\n");
	g_main_loop_run (loop);

	/* cleanup the stream */
	skyhub_stream_stop(stream);

	/* Out of the main loop, clean up nicely */
	return rc;
}

