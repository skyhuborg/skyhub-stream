# Sky Hub Stream

Sky Hub stream utilizes Deepstream and Deepstream to process video streams from a variety of sources.   Any URI source available to gstreamer is also available in Sky Hub Stream.  The goal of this project is to allow video streams to be run through machine learning models to analyze video streams and generate messages to be sent to the Sky Hub Tracker to notify the tracker of motion or the detection of objects in the video stream.

## Dependencies
*  NVidia Jetson Platform (Nano, Xavier AGX)
*  Deepstream 4.0
*  JetPack SDK 4.3


## Building
```
git clone git@gitlab.com:skyhuborg/skyhub-stream.git
cd skyhub-stream
mkdir build
cd build
cmake ..
make
```

## Running Sky Hub Stream
```
./build/apps/skyhub-stream --camera_type=fisheye --source=rtsp://<user>:<pass>@192.168.1.100:554
