#ifndef _SKYHUB_STREAM
#define _SKYHUB_STREAM

#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
#include <errno.h>

#include "gstnvdsmeta.h"
#ifndef PLATFORM_TEGRA
#include "gst-nvmessage.h"
#endif

typedef enum {
	SKYHUB_STREAM_TYPE_FISHEYE = 0,
	SKYHUB_STREAM_TYPE_PTZ = 1,
} SkyhubStreamType;

#define SKYHUB_FISHEYE_MAX 1
#define SKYHUB_PTZ_MAX 1


/**
 * @ Sky Hub Stream handle, object that represents a Sky Hub stream.
 * 
 * All Streams will be represent by a skyhub_stream_t object.  
 *
 */
typedef struct _skyhub_stream_t {
	SkyhubStreamType type; /**< Specifies if the stream is from a PTZ or Fish Eye camera */
	gint bus_watch_id; /**< Id for the Gstreamer Bus */
	GMainLoop *loop; /**< GMainLoop Object from the main loop */

	GstElement *pipeline; /**< Top level Gstreamer Pipeline object */

	gchar *active_model; /**< Specifies which machine-learning is currently active in the stream object */

	gchar *model_path; /**< Not implemented or used */

	int max_sources; /* max sources in stream */
	GList *sources; /* List of input sources attached to the stream */
} skyhub_stream_t;

/**
 *
 * @brief Create a new skyhub_stream_t object.
 *
 * This is the core object of Sky Hub Stream.
 *
 * @param stream_type Specifies if the input stream is from a PTZ or Fish Eye camera
 * @param loop Pointer to the GMainLoop object in your application.
 * @param detector Machine learning model that will be used on this stream.
 */
skyhub_stream_t * skyhub_stream_new(SkyhubStreamType stream_type, GMainLoop *loop, char *detector);

/**
 *
 * @brief Start processing the pipeline in the skyhub_stream_t object.
 *
 * This will initialize the full pipeline and all sources and start running 
 * data through the entire pipeline.
 *
 * @param stream pointer to an allocated and pre-configured skyhub_stream_t object
 */
int skyhub_stream_start(skyhub_stream_t *stream);

/**
 *
 * @brief Add an URI input source to the skyhub_stream_t object
 *
 * This will an input using a URI to the skyhub_stream_t object.  Any source uri 
 * supported by your Gstreamer will work. e.g. rtsp:// or file://
 *
 * @param stream pointer to a skyhub_stream_t object
 * @param uri uri to add as an input source
 */
int skyhub_stream_source_add(skyhub_stream_t *stream, const char *uri);

/**
 *
 * @brief Stop all processing on the skyhub_stream_t
 *
 * This will shut down all input sources and pipelines on the skyhub_stream_t,
 * this will effectively shutdown skyhub_stream_t.
 *
 * @param stream pointer to a skyhub_stream_t object
 */
void skyhub_stream_stop(skyhub_stream_t *stream);

/**
 *
 * @brief Releases all memory owned by the skyhub_stream_t
 *
 * This will remove all references and free all memory associated with the skyhub_stream_t
 *
 * @param stream pointer to a skyhub_stream_t object
 */
void skyhub_stream_free(skyhub_stream_t *stream);

/**
 *
 * @brief Set the active model in the skyhub_stream_t
 *
 * Set the active inference model that will be used in the analysis
 * pipeline in the skyhub_stream_t.  All video data will be processed
 * through this model. See skyhub_stream_list_models() for available models.

 *
 * @param stream Point to a skyhub_stream_t object.
 * @param model string that indicates which model to use.  */
gboolean skyhub_stream_set_model(skyhub_stream_t *stream, gchar *model);



/**
 *
 * @brief Return list of available models.
 *
 * Return a GList with strings indicating which models can be use
 * in the skyhub_stream_t.
 */
GList *skyhub_stream_list_models();


/**
 *
 * @brief Check to see if provided model is available.
 *
 * Detailed
 */
gboolean skyhub_stream_is_valid_model(gchar *name);

#endif

