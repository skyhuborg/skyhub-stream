#ifndef _SKYHUB_UTIL_H
#define _SKYHUB_UTIL_H

#include <gst/gst.h>
#include <glib.h>

GstElement * create_element(const char *factory, const char *name);
GstElement *create_pipeline(const char *name);

#endif
